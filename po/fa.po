# Persian translation for ubuntu-calculator-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-calculator-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calculator-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-28 13:08+0000\n"
"PO-Revision-Date: 2023-01-04 15:29+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Persian <https://hosted.weblate.org/projects/lomiri/lomiri-"
"calculator-app/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2017-04-05 07:42+0000\n"

#: lomiri-calculator-app.desktop.in:3 app/lomiri-calculator-app.qml:285
msgid "Calculator"
msgstr "ماشین‌حساب"

#: lomiri-calculator-app.desktop.in:4
#, fuzzy
#| msgid "A calculator for Ubuntu."
msgid "A calculator for Lomiri."
msgstr "ماشین‌حسابی برای اویونتو"

#: lomiri-calculator-app.desktop.in:5
msgid "math;addition;subtraction;multiplication;division;"
msgstr "math;addition;subtraction;multiplication;division;"

#: lomiri-calculator-app.desktop.in:7
#, fuzzy
msgid "/usr/share/lomiri-calculator-app/lomiri-calculator-app.svg"
msgstr "/usr/share/lomiri-calculator-app/lomiri-calculator-app.svg"

#: app/engine/formula.js:179
msgid "NaN"
msgstr "عدد نیست"

#: app/lomiri-calculator-app.qml:294
msgid "Cancel"
msgstr "لغو"

#: app/lomiri-calculator-app.qml:306
msgid "Select All"
msgstr "انتخاب همه"

#: app/lomiri-calculator-app.qml:306
msgid "Select None"
msgstr "انتخاب هیچ‌کدام"

#: app/lomiri-calculator-app.qml:313 app/lomiri-calculator-app.qml:429
msgid "Copy"
msgstr "رونوشت"

#: app/lomiri-calculator-app.qml:321 app/lomiri-calculator-app.qml:416
msgid "Delete"
msgstr "حذف"

#: app/lomiri-calculator-app.qml:439
msgid "Edit"
msgstr "ویرایش"

#: app/lomiri-calculator-app.qml:452
#, fuzzy
#| msgid "Edit formula"
msgid "Edit Result"
msgstr "ویرایش فرمول"

#: app/lomiri-calculator-app.qml:823
#, fuzzy
msgid "Functions"
msgstr "Functions"

#. TRANSLATORS Natural logarithm symbol (logarithm to the base e)
#: app/ui/BottomEdgePage.qml:55 app/ui/LandscapeKeyboard.qml:39
msgid "log"
msgstr "log"

#. TRANSLATORS Modulo operation: Finds the remainder after division of one number by another
#: app/ui/BottomEdgePage.qml:59 app/ui/LandscapeKeyboard.qml:46
msgid "mod"
msgstr "mod"

#~ msgid "Favorite"
#~ msgstr "برگزیده"

#~ msgid "Add to favorites"
#~ msgstr "افزودن به برگزیده‌ها"

#~ msgid "dd MMM yyyy"
#~ msgstr "dd MMMM yyyy"

#~ msgid "No favorites"
#~ msgstr "هیچ مورد برگزیده‌ای نیست"

#~ msgid ""
#~ "Swipe calculations to the left\n"
#~ "to mark as favorites"
#~ msgstr ""
#~ "برای علامت‌گذاری به عنوان برگزیده\n"
#~ "محاسبات را به چپ بکشید"

#~ msgid "Just now"
#~ msgstr "هم‌اکنون"

#~ msgid "Today "
#~ msgstr "امروز "

#~ msgid "hh:mm"
#~ msgstr "hh:mm"

#~ msgid "Yesterday"
#~ msgstr "دیروز"

#~ msgid "Skip"
#~ msgstr "پرش"

#~ msgid "Welcome to Calculator"
#~ msgstr "به ماشین‌حساب خوش آمدید"

#~ msgid "Enjoy the power of math by using Calculator"
#~ msgstr "با استفاده از اشین‌حساب، از قدرت ریاضی لذّت ببرید"

#~ msgid "Copy formula"
#~ msgstr "رونوشت از فرمول"

#~ msgid "Long press to copy part or all of a formula to the clipboard"
#~ msgstr "برای رونوشت تمام یا بخشی از فرمول به حافظه، نگه دارید"

#~ msgid "Enjoy"
#~ msgstr "لذّت ببرید"

#~ msgid "We hope you enjoy using Calculator!"
#~ msgstr "امیداوریم از استفاده از ماشین‌حساب لذّت ببرید!"

#~ msgid "Finish"
#~ msgstr "پایان"

#~ msgid "Scientific keyboard"
#~ msgstr "صفحه‌کلید مهندسی"

#~ msgid "Access scientific functions with a left swipe on the numeric keypad"
#~ msgstr "با کشیدن به چپ روی صفحه شماره، به تابع‌های مهندسی دسترسی پیدا کتید"

#~ msgid "Scientific View"
#~ msgstr "نمای مهندسی"

#~ msgid "Rotate device to show numeric and scientific functions together"
#~ msgstr "برای نمایش هم‌زمان عددها و تابع‌های مهندسی، افزار را بچرخانید"

#~ msgid "Delete item from calculation history"
#~ msgstr "حذف مورد از تاریخچهٔ محاسبه"

#~ msgid "Swipe right to delete items from calculator history"
#~ msgstr "برای حذف موارد از تاریخچهٔ ماشین‌حساب، به راست بکشید"

#~ msgid "Delete several items from calculation history"
#~ msgstr "حذف چند مورد از تاریخچهٔ محاسبه"

#~ msgid "Long press to select multiple calculations for deletion"
#~ msgstr "برای گزینش چند محاسبه به منظور حذف، نگه دارید"

#~ msgid "Delete whole formula at once"
#~ msgstr "حذف یک‌بارهٔ تمام فرمول"

#~ msgid "Long press '←' button to clear all formulas from input bar"
#~ msgstr "برای پاک سازی تمام فرمول‌ها از نوار ورودی، دکمهٔ «←» را نگه دارید"

#~ msgid "Edit item from calculation history"
#~ msgstr "ویرایش مورد از تاریخچهٔ محاسبه"

#~ msgid "Swipe left and press pencil to edit calculation"
#~ msgstr "برای ویرایش محاسبه، به چپ کشیده و مداد را بزنید"

#~ msgid "Add new favourite"
#~ msgstr "افزودن مورد دلخواه جدید"

#~ msgid "Swipe left and press star to add calculations to favourites view"
#~ msgstr "برای افزودن محاسبه‌ها به نمای بزگزیده، به چپ کشیده و ستاره را بزنید"

#~ msgid "Click in the middle of a formula to edit in place"
#~ msgstr "برای ویرایش درجای یک فرمول، در وسط آن کلیک کنید"
